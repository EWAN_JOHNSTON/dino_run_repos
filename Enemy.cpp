#include "Enemy.h"
#include "AssetManager.h"

Enemy::Enemy()
	:SpriteObject(AssetManager::RequestTexture("Assets/cactus.png"))
	, position(2000, 720)
	, velocity(200)
{
	sprite.setPosition(position);
}

Enemy::Enemy(sf::Texture& newTexture, sf::Vector2f newPosition)
	:SpriteObject(newTexture)
	, velocity(200)
{
	sprite.setPosition(newPosition);
}

void Enemy::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	sf::Vector2f newPos = sprite.getPosition();
	newPos.x -= velocity * frameTime.asSeconds();
	sprite.setPosition(newPos);
}